package tgpr.tuto.controller;

import tgpr.tuto.model.Member;
import tgpr.tuto.view.ProfileView;
import tgpr.tuto.view.View;

public class ProfileController extends Controller {
    private final Member member;
    private final ProfileView view;

    public ProfileController(Member member) {
        this.member = member;
        this.view = new ProfileView(member);
    }

    @Override
    public void run() {
        View.Action res;
        try{
            do{
                view.displayHeader();;
                view.displayProfile();
                res = view.askForAction();
                switch (res.getAction()){
                    case 'U':
                        new ProfileUpdateController(member).run();
                        break;
                }
            } while (res.getAction() != 'L');
        } catch (View.ActionInterruptedException e){
            // just leave
        }
    }
}
