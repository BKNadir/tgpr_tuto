package tgpr.tuto.controller;

import tgpr.tuto.model.Member;
import tgpr.tuto.view.MemberListView;
import tgpr.tuto.view.View;

public class MemberListController extends Controller {
    @Override
    public void run() {
        var members = Member.getAll();
        var view = new MemberListView();
        view.displayHeader();
        view.displayMembers(members);
        try {
            View.Action res;
            do {
                view.displayHeader();
                view.displayMembers(members);
                res = view.askForAction(members.size());
                switch (res.getAction()) {
                    case 'V':
                        var m = members.get(res.getNumber() - 1);
                        new ProfileController(m).run();
                        break;
                }
            } while (res.getAction() != 'L');
        } catch (View.ActionInterruptedException e) {
            // just leave the loop
        }
        view.pausedWarning("Leaving the application");
    }
}