package tgpr.tuto.controller;

import tgpr.tuto.model.Member;
import tgpr.tuto.view.ProfileUpdateView;
import tgpr.tuto.view.View;
import java.time.LocalDate;

public class ProfileUpdateController extends Controller {
    private final Member member;
    private final ProfileUpdateView view = new ProfileUpdateView();

    public ProfileUpdateController(Member member){
        this.member = member;
    }

    public void run(){
        view.displayHeader();
        View.Action res;
        try {
            view.displayPseudo(member.getPseudo());
            String profile = view.askProfile(member.getProfile());
            boolean admin = view.askAdmin(member.isAdmin());
            LocalDate birthDate = view.askBirthDate(member.getBirthdate());

            res = view.askForAction();
            if (res.getAction() == 'O') {
                member.setProfile(profile);
                member.setAdmin(admin);
                member.setBirthdate(birthDate);
                member.save();
            }
        }
        catch (View.ActionInterruptedException e){
            view.pausedWarning("update profile aborded");
        }
    }
}
