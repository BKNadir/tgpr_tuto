package tgpr.tuto.controller;

public abstract class Controller {
    public abstract void run();
}