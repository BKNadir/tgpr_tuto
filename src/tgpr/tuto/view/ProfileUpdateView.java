package tgpr.tuto.view;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
public class ProfileUpdateView extends View {

    public void displayHeader() {
        clear();
        println("\n=== Update Profile ===\n");
    }

    public void displayPseudo(String pseudo) {
        println("Pseudo: " + pseudo);
    }

    public String askProfile(String actual) {
        return askString("Profile (" + actual + "): ", actual);
    }

    public LocalDate askBirthDate(LocalDate actual) {
        return askDate("Birth Date (" +
                (actual == null ? "null" : DateTimeFormatter.ofPattern("dd/MM/yyyy").format(actual)) + "): ", actual);
    }

    public boolean askAdmin(boolean actual) {
        return askBoolean("Is Admin(" + actual + "): ", actual);
    }

    public View.Action askForAction(){
        return doAskForAction(-1, "\n[O] Confirm, [C] Cancel", "[oO]|[cC]");
    }
}
