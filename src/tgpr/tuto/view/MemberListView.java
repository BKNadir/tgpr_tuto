package tgpr.tuto.view;

import tgpr.tuto.model.Member;

import java.util.List;

public class MemberListView extends View {

    public void displayHeader() {
        clear();
        println("\n=== Member List ===\n");
    }

    public void displayMembers(List<Member> members) {
        int i = 1;
        for (var m : members) {
            printf("[%2d] %s\n", i, m.getPseudo());
            ++i;
        }
    }

    public View.Action askForAction(int size) {
        return doAskForAction(size, "\n[V] View, [L] Leave","[vV][0-9]+|[lL]");
    }
}