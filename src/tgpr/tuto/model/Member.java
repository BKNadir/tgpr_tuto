package tgpr.tuto.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;

public class Member extends Model{
    private String pseudo;
    private String password;
    private String profile;
    private boolean admin;
    private LocalDate birthdate;

    public Member(String pseudo, String password, String profile, boolean admin, LocalDate birthdate) {
        this.pseudo = pseudo;
        this.password = password;
        this.profile = profile;
        this.admin = admin;
        this.birthdate = birthdate;
    }

    public Member(String pseudo, String password, boolean admin) {
        this.pseudo = pseudo;
        this.password = password;
        this.admin = admin;
    }

    public Member() {
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public String toString() {
        return "tgpr.tuto.model.Member{" +
                "pseudo='" + pseudo + '\'' +
                ", profile='" + profile + '\'' +
                ", admin=" + admin +
                ", birthdate=" + birthdate +
                '}';
    }

    public static void mapper(ResultSet rs, Member member) throws SQLException {
        member.pseudo = rs.getString("pseudo");
        member.password = rs.getString("password");
        member.profile = rs.getString("profile");
        member.admin = rs.getBoolean("admin");
        member.birthdate = rs.getObject("birthdate", LocalDate.class);
    }

    public static List<Member> getAll() {
        var list = new ArrayList<Member>();
        try {
            var stmt = db.prepareStatement("select * from members order by pseudo");
            var rs = stmt.executeQuery();
            while (rs.next()) {
                var member = new Member();
                mapper(rs, member);
                list.add(member);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Member getByPseudo(String pseudo) {
        Member member = null;
        try {
            var stmt = db.prepareStatement("select * from members where pseudo=?");
            stmt.setString(1, pseudo);
            var rs = stmt.executeQuery();
            if (rs.next()) {
                member = new Member();
                mapper(rs, member);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return member;
    }

    public boolean save() {
        Member m = getByPseudo(pseudo);
        int count = 0;
        try {
            PreparedStatement stmt;
            if (m == null) {
                stmt = db.prepareStatement("insert into members (pseudo, password, profile, admin, birthdate) values (?,?,?,?,?)");
                stmt.setString(1, pseudo);
                stmt.setString(2, password);
                stmt.setString(3, profile);
                stmt.setBoolean(4, admin);
                stmt.setObject(5, birthdate);
            } else {
                stmt = db.prepareStatement("update members set password=?, profile=?, admin=?, birthdate=? where pseudo=?");
                stmt.setString(1, password);
                stmt.setString(2, profile);
                stmt.setBoolean(3, admin);
                stmt.setObject(4, birthdate);
                stmt.setString(5, pseudo);
            }
            count = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return count == 1;
    }

    public boolean delete() {
        int count = 0;
        try {
            PreparedStatement stmt = db.prepareStatement("delete from members where pseudo=?");
            stmt.setString(1, pseudo);
            count = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return count == 1;
    }
}
